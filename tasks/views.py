from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from tasks.models import Task
from tasks.forms import TaskForm
from projects.models import Project
from projects.views import show_project
from tasks.models import Task


@login_required
def create_task(request):
    return CreateView.as_view(
        model=Task,
        form_class=TaskForm,
        template_name="tasks/create_task.html",
        success_url=reverse_lazy("show_project", kwargs={"id": Project.id}),
    )(request)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/show_my_tasks.html", context)
