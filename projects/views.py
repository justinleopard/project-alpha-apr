from django.shortcuts import render, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from projects.forms import ProjectForm
from django.views.generic.edit import CreateView


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project_detail = get_object_or_404(Project, id=id)
    context = {"project_detail": project_detail}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    return CreateView.as_view(
        model=Project,
        form_class=ProjectForm,
        template_name="projects/create_project.html",
        success_url=reverse_lazy("list_projects"),
    )(request)
